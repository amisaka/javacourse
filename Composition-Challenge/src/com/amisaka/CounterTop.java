package com.amisaka;

/**
 * Created by amisaka on 02/06/17.
 */
public class CounterTop {
    private String manufacturer;
    private String model;
    private String material;
    private Dimensions dimensions;
    private Sink sinkType;

    public CounterTop(String manufacturer, String model, String material, Dimensions dimensions) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.material = material;
        this.dimensions = dimensions;
    }


    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getMaterial() {
        return material;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }
}
