package com.amisaka;

/**
 * Created by amisaka on 02/06/17.
 */
public class Kitchen {
    private Dimensions kitchenSize;
    private CounterTop countertop;
    private Sink kitchenSink;
    private theTable table;

    public Kitchen(Dimensions kitchenSize, CounterTop countertop, Sink kitchenSink, theTable table) {
        this.kitchenSize = kitchenSize;
        this.countertop = countertop;
        this.kitchenSink = kitchenSink;
        this.table = table;
    }

    public void drain() {
        System.out.println("Draining water");
        kitchenSink.drainWater(true);
    }


    public Dimensions getKitchenSize() {
        return kitchenSize;
    }

    public CounterTop getCountertop() {
        return countertop;
    }

    public theTable gettheTable() {
        return this.table;
    }

    public Sink getKitchenSink() {
        return  this.kitchenSink;
    }


}
