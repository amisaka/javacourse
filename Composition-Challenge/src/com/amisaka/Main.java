package com.amisaka;

public class Main {

    public static void main(String[] args) {
        // Create a single room of a house using composition
        // Think about the things that should be included in the room
        // Maybe physical parts of the house but furniture as well
        // Add at least one method to access an object via a getter and
        // then that objects public method as you saw in the previous video
        // the add at least one method to hide the object e.g. not using a getter
        // but to access the object used in composition within the main class
        // like you saw in this video

        Dimensions counterTopSize = new Dimensions(10,4,3);
        Dimensions kitchenSize = new Dimensions(15,8,15);
        Dimensions tableSize = new Dimensions(6,4,4);
        theTable kitchenTable = new theTable(tableSize,"wood",4);
        Sink theSink = new Sink("Bloob","ACMe",2,10);
        CounterTop kitchenCounterTop = new CounterTop("Bella","NG","granite", counterTopSize);
        Kitchen myKitchen = new Kitchen(kitchenSize,kitchenCounterTop,theSink,kitchenTable);
        myKitchen.gettheTable().extendTable(true);
        myKitchen.drain();

    }
}
