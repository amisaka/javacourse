package com.amisaka;

/**
 * Created by amisaka on 02/06/17.
 */
public class Sink {
    private String model;
    private String manufacturer;
    private int numberCubes;
    private int waterLevel;

    public Sink(String model, String manufacturer, int numberCubes, int waterLevel) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.numberCubes = numberCubes;
        this.waterLevel = waterLevel;
    }

    public void drainWater(boolean isDraining){
        if (getWaterLevel() > 0 && isDraining) {
            System.out.println("Water is draining...");
            this.waterLevel = 0;
        } else {
            System.out.println("No water to be drained.");
        }

    }

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getNumberCubes() {
        return numberCubes;
    }

    public int getWaterLevel() {
        return waterLevel;
    }
}
