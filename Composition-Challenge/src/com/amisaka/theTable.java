package com.amisaka;

/**
 * Created by amisaka on 02/06/17.
 */
public class theTable {
    private Dimensions tableSize;
    private String madeOf;
    private int numberChairs;
    private boolean tableExtended;

    public theTable(Dimensions tableSize, String madeOf, int numberChairs) {
        this.tableSize = tableSize;
        this.madeOf = madeOf;
        this.numberChairs = numberChairs;
    }

    public void extendTable(boolean extend) {
        if (extend && !this.tableExtended) {
            System.out.println("Table is extended");
            this.tableExtended = true;
        } else {
            System.out.println("Table has maximum length");
        }
    }
    public Dimensions getTableSize() {
        return tableSize;
    }

    public String getMadeOf() {
        return madeOf;
    }

    public int getNumberChairs() {
        return numberChairs;
    }

    public boolean isTableExtended() {
        return tableExtended;
    }
}
