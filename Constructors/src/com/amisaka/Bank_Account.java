package com.amisaka;

/**
 * Created by amisaka on 31/05/17.
 */
public class Bank_Account {
    private int accountNumber;
    private double balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    public Bank_Account() {
        this(9999,0.0,"Default name","Default email","Default Phone number" );
        System.out.println("Empty constructor called");
    }

    public Bank_Account(int accountNumber, double balance,
                        String customerName, String email, String phoneNumber) {
        System.out.println("Account constructor with parameters called.");
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public void depositFunds(double fund) {
        this.balance += fund;
        System.out.println("Deposit of " + fund + " made");
        System.out.println("Current balance: " + this.balance);
    }

    public void withdrawFunds(double money) {
        if (this.balance >= money) {
            System.out.println("Withdraw is " + money);
            this.balance -= money;
            System.out.println("New Balance is " + this.balance);
        } else {
            System.out.println("Withdraw is " + money);
            System.out.println("Balance is " + this.balance);
            System.out.println("Insufficient funds!!!");
        }
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getAccountNumber() {
        return this.accountNumber;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }


}
