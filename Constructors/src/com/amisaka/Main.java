package com.amisaka;

import com.sun.org.apache.xpath.internal.SourceTree;

public class Main {

    public static void main(String[] args) {
        // CHALLENGE #1
	    // Create a new class for a bank account
        // Create fields for the account number, balance, customer name, email and phone number.
        //
        // Create getters and setters for each field
        // Create two additional methods
        // 1. To allow the customer to deposit funds (this should increment the balance field.
        // 2. To allow the customer to withdraw funds. This should deduct from the balance field.
        // but not allow the withdrawal to complete if their are insufficient funds.
        // You will want to create various code in the Main class (the one created by IntelliJ) to
        // confirm your code is working
        // Add some System.out.println's in the two methods above as well.

 /*       Bank_Account bAcc1 = new Bank_Account(1234, 0.00, "John F", "jf@email.com", "555-3434");


        System.out.println("Account number: "+bAcc1.getAccountNumber());
        System.out.println("Customer name: " + bAcc1.getCustomerName());
        System.out.println("Customer email: " + bAcc1.getEmail());
        System.out.println("Customer telephone number: " + bAcc1.getPhoneNumber());
        System.out.println("Account balance: " + bAcc1.getBalance());

        bAcc1.depositFunds(300);

        bAcc1.withdrawFunds(400.01);

        Bank_Account bAcc2 = new Bank_Account();
        System.out.println("Account number: "+bAcc2.getAccountNumber());
        System.out.println("Customer name: " + bAcc2.getCustomerName());
        System.out.println("Customer email: " + bAcc2.getEmail());
        System.out.println("Customer telephone number: " + bAcc2.getPhoneNumber());
        System.out.println("Account balance: " + bAcc2.getBalance());
*/

        // CHALLENGE #2
        // Create a new class VipCustomer
        // it should have 3 fields name, credit limit, and email address
        // create 3 constructors
        // 1st constructor empty should call the constructor with 3 parameters with default values
        // 2nd constructor should pass on the 2 values it receives and add a default value for the 3rd
        // 3rd constructor should save all fields
        // create getters only for this using code generation of intelliJ as setters wont be needed
        // test and confirm it works.

        VipCustomer vipJoe = new VipCustomer();
        System.out.println("VIP Customer Name: " + vipJoe.getVipCustName());
        System.out.println("VIP Customer Limit: " + vipJoe.getVipCustLimit());
        System.out.println("VIP Customer Email: " + vipJoe.getVipCustEmail());

        VipCustomer vipAnn = new VipCustomer("Ann B","ab@email.com");
        System.out.println("VIP Customer Name: " + vipAnn.getVipCustName());
        System.out.println("VIP Customer Limit: " + vipAnn.getVipCustLimit());
        System.out.println("VIP Customer Email: " + vipAnn.getVipCustEmail());

        VipCustomer vipScott = new VipCustomer("Scott A",2000d,"sa@email.com");
        System.out.println("VIP Customer Name: " + vipScott.getVipCustName());
        System.out.println("VIP Customer Limit: " + vipScott.getVipCustLimit());
        System.out.println("VIP Customer Email: " + vipScott.getVipCustEmail());





    }

}
