package com.amisaka;

/**
 * Created by amisaka on 01/06/17.
 */
public class VipCustomer {

    private String vipCustName;
    private double vipCustLimit;
    private String vipCustEmail;

    public VipCustomer() {
        this( "Customer Name", 500d, "desc@email.com");
    }

    public VipCustomer(String vipCustName, String vipCustEmail) {
        this(vipCustName, 1000d, vipCustEmail);
    }

    public VipCustomer(String vipCustName, double vipCustLimit, String vipCustEmail) {
        this.vipCustName = vipCustName;
        this.vipCustLimit = vipCustLimit;
        this.vipCustEmail = vipCustEmail;
    }

    public String getVipCustName() {
        return vipCustName;
    }

    public double getVipCustLimit() {
        return vipCustLimit;
    }

    public String getVipCustEmail() {
        return vipCustEmail;
    }
}
