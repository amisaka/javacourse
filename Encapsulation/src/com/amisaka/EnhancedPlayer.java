package com.amisaka;

/**
 * Created by amisaka on 05/06/17.
 * This code controls the access to the fields
 */
public class EnhancedPlayer {
    private String name;
    private int hitPoints = 100;
    private String weapon;

    public EnhancedPlayer(String name, int health, String weapon) {
        this.name = name;

        if (health >0 && health <= 100) {
            this.hitPoints = health;
        }
        this.weapon = weapon;
    }

    public void loseHealth(int damage) {
        this.hitPoints = this.hitPoints - damage;
        if (this.hitPoints <= 0) {
            System.out.println("Plyer knocked out");
            // Reduce number of lives remaining for the player
        }
    }

    public  int getHitPoints(){
        return hitPoints;
    }
}

