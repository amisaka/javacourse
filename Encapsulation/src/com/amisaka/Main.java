package com.amisaka;

public class Main {

    public static void main(String[] args) {
/*	    Player player = new Player();

	    player.name = "Antonio";    // error due a change on Player class
	    player.health = 20;
	    player.weapon = "Sword";

	    int damage = 10;

	    player.loseHealth(damage);
        System.out.println("Remaining health = " + player.healthRemaining());

        damage = 11;
        player.health = 200;   // it was not supposed to be changed from here
        player.loseHealth(damage);
		System.out.println("Remaining health = " + player.healthRemaining());
		*/

		EnhancedPlayer player = new EnhancedPlayer("Antonio", 200, "Sword");
		System.out.println("Initial health is " + player.getHitPoints());
	}
}
