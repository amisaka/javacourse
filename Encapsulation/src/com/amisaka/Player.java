package com.amisaka;

/**
 * Created by amisaka on 05/06/17.
 * Here this code allows change from outside of this class program
 */
public class Player {
    public String fullname;
    public int health;
    public String weapon;

    public void loseHealth(int damage) {
        this.health = this.health - damage;
        if (this.health <= 0) {
            System.out.println("Plyer knocked out");
            // Reduce number of lives remaining for the player
        }
    }

    public  int healthRemaining(){
        return this.health;
    }
}
