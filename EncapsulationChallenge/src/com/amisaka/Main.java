package com.amisaka;

public class Main {

    public static void main(String[] args) {
	    // Create a class and demonstrate proper encapsulation techniques
        // the class will be called Printer
        // It will simulate a real Computer Printer
        // It should have fields for the toner level, number of pages printed, and
        // also whether its a duplex printer (capable of printing on both sides of the paper).
        // Add methods to fill up the toner (which should increase the number of pages printed).
        // Simulate printing a page (which should increase the number of pages printed).
        // Decide on the scope, whether to use constructor, and anything else you think is needed.


        Printer printer1 = new Printer(50, true);

        for (int i=0; i<30;i++) {
            printer1.printingPage(true);
            printer1.printingPage(false);
            if (printer1.getTonerLevel()<= 0) {
                printer1.addToner(100);
            }
            System.out.println("Number of pages printed = " + printer1.getNumberOfPagesPrinted());
        }
    }

}
