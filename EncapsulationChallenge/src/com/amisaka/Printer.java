package com.amisaka;

/**
 * Created by amisaka on 05/06/17.
 */
public class Printer {
    private int tonerLevel = 100;
    private int numberOfPagesPrinted = 0;
    private boolean isDuplex = false;

    public Printer(int tonerLevel,  boolean isDuplex) {
        this.tonerLevel = tonerLevel;
        this.numberOfPagesPrinted = 0;
        this.isDuplex = isDuplex;
    }

    public void addToner(int tonerAmount) {
        if (this.tonerLevel < 0 && (this.tonerLevel + tonerAmount) < 100) {
            this.tonerLevel += tonerAmount;
            System.out.println("Remaining toner level " + getTonerLevel());
        } else {
            System.out.println("To much toner to fill up...");
        }

    }

    public void printingPage(boolean isDoublePage) {
        if (this.tonerLevel >= 0) {
            if (this.isDuplex && isDoublePage) {
                if ((this.tonerLevel +2) >= 0) {
                    this.numberOfPagesPrinted += 2;
                    this.tonerLevel -= 2;
                    System.out.println("Toner level: " + getTonerLevel());
                }
            } else {
                if ((this.tonerLevel + 1) >=0 ) {
                    this.numberOfPagesPrinted += 1;
                    this.tonerLevel -= 1;
                    System.out.println("Toner level: " + getTonerLevel());
                }
            }
        } else {
                System.out.println("Toner level is empty!");
            }
        }


    public int getTonerLevel() {
        return this.tonerLevel;
    }

    public int getNumberOfPagesPrinted() {
        return this.numberOfPagesPrinted;
    }
}
