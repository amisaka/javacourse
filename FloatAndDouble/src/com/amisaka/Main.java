package com.amisaka;

public class Main {

    public static void main(String[] args) {

        // width of int = 32 (4 bytes).
        int myIntValue = 5/3;
        // width of float = 32 (4 bytes).
        float myFloatValue = 5f /3f;  // (double) 5.4
        // width of double = 64 (8 bytes).
        double myDoubleValue = 5d /3d;

        System.out.println("myIntValue = " + myIntValue);
        System.out.println("myFloatValue = " + myFloatValue);
        System.out.println("myDoubleValue = " + myDoubleValue);

        // convert a given number of pounds to kilograms
        // 1. Create a variable to store the number of pounds
        // 2. Calculate the number of kilograms for the number above and store in a variable
        // 3. Print out the result.

        double poundsValue = 200d;
        double kiloValue = poundsValue * 0.45359237d;
        System.out.println("Value of "+poundsValue+" pounds in kilograms is "+kiloValue+" kilograms");
        double pi = 3.141_592_7d;
    }
}
