package com.amisaka;

/**
 * Created by amisaka on 01/06/17.
 */
public class Car extends Vehicle {

    private String bodyType;
    private int fuelType;

    public Car(String make, String model, int maxSpeed, int capacity,
               int currentSpeed, String bodyType, int fuelType) {
        super(make, model, maxSpeed, capacity, currentSpeed);
        this.bodyType = bodyType;
        this.fuelType = fuelType;
    }

    public Car(String make, String model, int maxSpeed, int capacity, String bodyType, int fuelType) {
        super(make, model, maxSpeed, capacity, 0);
        this.bodyType = bodyType;
        this.fuelType = fuelType;
    }

    public Car(){
        this("Unknown","Luxo",200,4,"hatch",1);
    }

    @Override
    public void handSteering() {
        System.out.println("Car.handSteering() was called.");
        super.handSteering();
    }

    public void savePower(){
        System.out.println("Car.savePower() was called.");
    }
}
