package com.amisaka;

/**
 * Created by amisaka on 01/06/17.
 */
public class ElectricCar extends Car {

    private int electricMotors;
    private int batteryPower;

    public ElectricCar(String make, String model, int maxSpeed, int capacity, int currentSpeed,
                       String bodyType, int fuelType, int electricMotors, int batteryPower) {
        super(make, model, maxSpeed, capacity, currentSpeed, bodyType, fuelType);
        this.electricMotors = electricMotors;
        this.batteryPower = batteryPower;
    }


    public ElectricCar(){
        this("Tesla","S10",220,5,0,"sedan",3,4,8);
    }
    @Override
    public void savePower() {
        System.out.println("ElectricCar.savePower() was called.");
        super.savePower();
    }

    public void changingGears(int ratio) {
        System.out.println("Changing the speed ratio " + ratio );
        this.setCurrentSpeed(ratio*5);
    }

    public void accelerate(int rate) {
        int newVelocity = getCurrentSpeed() + rate;
        if (newVelocity == 0) {
            stop();
        } else if (newVelocity >0 && newVelocity <= 10) {
            changingGears(1);
        } else if (newVelocity >10 && newVelocity <= 20) {
            changingGears(2);
        } else if (newVelocity > 20 && newVelocity <=30) {
            changingGears(3);
        } else {
            changingGears(4);
        }
    }
}
