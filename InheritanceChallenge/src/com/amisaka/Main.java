package com.amisaka;

public class Main {

    public static void main(String[] args) {
	    // Challenge
        // Start with a base class of Vehicle, then create a Car class that inherits from this base class.
        // Finally, create another class, a specific type of Car that inherits from the Car.
        // You should be able to hand steering, changing gears, and moving (speed in other words).
        // You will want to decide where to put the appropriate state and behaviours (fields and methods).
        // As mentioned above, changing gears, increasing/decreasing speed should be included.
        // For your specific type of vehicle you will want to add something specific for that type of car.

        Vehicle bicycle = new Vehicle();
        bicycle.handSteering();

        Car sedan = new Car();
        sedan.savePower();

        ElectricCar zip = new ElectricCar();
        zip.savePower();
        zip.accelerate(50);
    }
}
