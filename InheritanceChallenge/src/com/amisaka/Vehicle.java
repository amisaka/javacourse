package com.amisaka;

/**
 * Created by amisaka on 01/06/17.
 */
public class Vehicle {
    private String make;
    private String model;
    private int maxSpeed;
    private int capacity;
    private int currentSpeed;

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public Vehicle(String make, String model, int maxSpeed, int capacity, int currentSpeed) {
        this.make = make;
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.capacity = capacity;
        this.currentSpeed = currentSpeed;
    }

    public Vehicle(){
        this( "Manufacturer","Model",30,2,10
        );
    }

    public void stop() {
        this.currentSpeed = 0;
    }

    public void handSteering() {
        System.out.println("Vehicle.handSteering() was called.");
    }

    public void changingGears() {
        System.out.println("Vehicle.changingGears() was called.");
    }

    public void moving() {
        System.out.println("Vehicle.moving() was called");
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getCapacity() {
        return capacity;
    }
}
