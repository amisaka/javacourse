package com.amisaka;

public class Main {

    public static void main(String[] args) {
	// false, true, null - reserved key words
        // a mile is equal to 1.609344 kilometres
        double kilometres = (100 * 1.609344);
        int highScore = 50
        if(highScore == 50) {
            System.out.println("This is an expression");
        }


        // In the following code that I will type below, write down what parts of the code
        // are expressions.

        int score = 100;   // score = 100
        if (score > 99) {  // score > 99
            System.out.println("You got the high score!");   // "You got the high score!"
            score = 0;   // this is an expression
        }

    }
}
