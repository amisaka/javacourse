package com.amisaka;

public class Main {
/*
    public static void main(String[] args) {
	    int newScore = calculateScore("Tim",500);
        System.out.println("New score " + newScore);
        calculateScore(75);
        calculateScore();


    }

    public static int calculateScore(String playerName, int score) {
        System.out.println("Player " + playerName + " score " + score + " points");
        return score * 1000;
    }
    public static int calculateScore(int score) {
        System.out.println("Unamed player score " + score + " points");
        return score * 1000;
    }
    public static int calculateScore() {
        System.out.println("No player name, no player points");
        return 0;
    }
*/

    public static void main(String[] args) {
        // Create a method called calcFeetAndInchesToCentimeters
        // It needs to have two parameters.
        // feet is the first parameter, inches is the 2nd parameter
        //
        // You should validate that the first parameter feet is >= 0
        // You should validate that the 2nd parameter inches is >=0 and <=12
        // return -1 from the method if either of the above is not true
        //
        // If the parameters are valid, then calculate how many centimeters
        // comprise the feet and inches passed to this method and return that value
        //
        // Create a second method of the same name but with only the parameter
        // inches is the parameter
        // validate that its >=0
        // return -1 if it is not true
        // But if its valid, then calculate how many feet are in the inches
        // and then here is the trick part
        // call the other overloaded method passing the correct feet and inches
        // calculated so that it can calculate correctly
        // hints: use double for your number datatypes is probably a good idea
        // 1 inch = 2.54 cm and one foot = 12 inches
        // use the link I give you to confirm your code is calculating correctly.
        // Calling another overloaded method just requires you to use the
        // right number of parameters.

        double feetValue = 6;
        double inchValue = 12;
        calcFeetAndInchesToCentimeters(feetValue,inchValue);


        double newInch = 48;
        calcFeetAndInchesToCentimeters(newInch);

    }

    public static double calcFeetAndInchesToCentimeters(double feetValue, double inchValue) {
        /*
        if (feetValue >= 0 && (inchValue >= 0 && inchValue <= 12d )) {
            double cmValue = ((feetValue * 12) + inchValue) * 2.54;
            return cmValue;
        } else {
            return -1;
        }
        */
        if (feetValue < 0 || (inchValue < 0 || inchValue > 12d )) {
            System.out.println("One or both values are invalid!");
            return -1;
        }
        double cmValue = ((feetValue * 12) + inchValue) * 2.54;
        System.out.println(feetValue + " feet "
                            + inchValue + " inches = "
                            + cmValue + " centimeters");
        return cmValue;


    }

    public static double calcFeetAndInchesToCentimeters(double inchValue) {
        if (inchValue < 0  ) {
            System.out.println("An invalid value in inches has entered!");
            return -1;
        }
        double calcFeet = (int) inchValue/12;
        double remainInches =(int) inchValue % 12;
        System.out.println(inchValue + " inches is equal to "
                            + calcFeet + " feet "
                            + remainInches + " inches");
        return calcFeetAndInchesToCentimeters(calcFeet,remainInches);

        /*
        double calcInchResult = calcFeetAndInchesToCentimeters( calcFeet, remainInches);

        System.out.println("Calculation of "
                + inchValue
                + " inches to centimeters is "
                + calcInchResult
                + " centimeters" );

        return 1;
        */
    }


}
