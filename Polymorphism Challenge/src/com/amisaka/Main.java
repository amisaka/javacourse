package com.amisaka;

class Car{
    private String name = "";
    private boolean engine;
    private int cylinders;
    private int wheels;
    private int speedCar=0;
    private boolean isBraked;

    public int getSpeedCar() {
        return this.speedCar;
    }

    public String getName() {
        return name;
    }

    public int getCylinders() {
        return cylinders;
    }

    public Car(String name, int cylinders) {
        this.name = name;
        this.engine = true;
        this.cylinders = cylinders;
        this.wheels = 4;
    }

    public String startEngine() {
        return "Car -> startEngine()";
    }

    public String accelerate() {
        return "Car -> accelerate()";
    }

    public String brake() {
        return "Car -> brake()";
    }
/*
    }
    public void startEngine(boolean engineStarted) {
        this.engine = engineStarted;

        if (this.engine) {
            System.out.println("Engine is running.");
        } else {
            System.out.println("Engine is off.");
        }
    }

    public void accelerateCar(int speed) {
        this.speedCar += speed;
        System.out.println("Car speed is " + this.speed);

    }

    public void brakeCar(boolean isBraked) {
        this.isBraked = isBraked;
        if (isBraked) {
            System.out.println("The car is braked.");
        } else {
            System.out.println("The car is NOT braked");
        }
    }
*/


}

class Toyota extends Car {
    public Toyota(String name, int cylinders) {
        super(name, cylinders);
    }

    @Override
    public String startEngine() {
        return getClass().getSimpleName() + " -> startEngine()";
    }

    @Override
    public String accelerate() {
        return getClass().getSimpleName() + " -> accelerate()";
    }

    @Override
    public String brake() {
        return getClass().getSimpleName() + " -> brake()";
    }
}

class Honda extends Car {
    public Honda(String name, int cylinders) {
        super(name, cylinders);
    }

    @Override
    public String startEngine() {
        return "Honda -> startEngine()";
    }

    @Override
    public String accelerate() {
        return "Honda -> accelerate()";
    }

    @Override
    public String brake() {
        return "Honda -> brake()";
    }
}

class VW extends Car {
    public VW(String name, int cylinders) {
        super(name, cylinders);
    }

    @Override
    public String startEngine() {
        return "VW -> startEngine()";
    }

    @Override
    public String accelerate() {
        return "VW -> accelerate()";
    }

    @Override
    public String brake() {
        return "VW -> brake()";
    }
}


public class Main {

    public static void main(String[] args) {
        // We are going to go back to the car analogy
        // Create a base class called Car
        // It should have a few fields that would be appropriate for a generic car class
        // engine, cylinders, wheels, etc.
        // Constructor should initialize cylinders (number of) and name, and set wheels to 4
        // and engine to true. Cylinders and names would be passed parameters
        //
        // Create appropriate getters
        //
        // Create some methods like startEngine, accelerate, and brake
        //
        // show a message for each in the base class
        // Now create 3 sub classes for your favorite vehicles
        // Override the appropriate methods to demonstrate polymorphism in use.
        // put all classes in the one java file (this one).

        Car car = new Car("Base car", 8);
        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());

        Toyota toyota = new Toyota("Senza", 6);
        System.out.println(toyota.startEngine());
        System.out.println(toyota.accelerate());
        System.out.println(toyota.brake());

        VW vw = new VW("Beetle", 4);
        System.out.println(vw.startEngine());
        System.out.println(vw.accelerate());
        System.out.println(vw.brake());

        Honda honda = new Honda("Civic", 4);
        System.out.println(honda.startEngine());
        System.out.println(honda.accelerate());
        System.out.println(honda.brake());
    }


}
