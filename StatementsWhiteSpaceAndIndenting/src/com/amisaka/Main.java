package com.amisaka;

public class Main {

    public static void main(String[] args) {
        int myVariable = 50;  // entire line is an statement
        myVariable++;
        myVariable--;

        System.out.println("This is a test");

        System.out.println("This is " +
                "another" +
                " still more.");

        int anotherVariable = 50;
        myVariable--;
        System.out.println("This is another one");

    }
}
